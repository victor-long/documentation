# Remote storage (S3)

PeerTube supports streaming directly from an s3 public bucket. The integration
is done via FUSE, for instance with [s3fs](https://github.com/s3fs-fuse/s3fs-fuse).

```bash
export S3_STORAGE=/var/www/peertube/s3-storage
mkdir $S3_STORAGE
s3fs your-space-name /var/www/peertube/s3-storage -o url=https://region.digitaloceanspaces.com -o allow_other -o use_path_request_style -o uid=1000 -o gid=1000
mount --bind $S3_STORAGE/videos /var/www/peertube/storage/videos
mount --bind $S3_STORAGE/redundancy /var/www/peertube/storage/redundancy
mount --bind $S3_STORAGE/streaming-playlists /var/www/peertube/storage/streaming-playlists
```

?> **Note**: see https://github.com/Chocobozzz/PeerTube/issues/147 for a list of known
untested equivalents to s3fs.

Now set the base public url of your bucket in your reverse proxy, as shown in the
[official Nginx template](https://github.com/Chocobozzz/PeerTube/blob/5f59cf077fd9f9c0c91c7bb56efbfd5db103bff2/support/nginx/peertube#L235-L242).
Videos should now stream directly from the configured bucket, saving you some significant bandwidth!
